# Swatches for Version 3.8.3

This is a port from original version: [jodyheavener/Swatches](https://github.com/jodyheavener/Swatches)

We added a little bit of validation from the input and cleaned up the code as well.

## What does it do?
Once is installed, given a list of hex colors the plugin will generate a palette of color swatches.

## How to use

* First download this zip file -> [Link](https://gitlab.com/viraltree/swatches/repository/archive.zip?ref=master)
* Unzip the file and open the new directory created
* Double click on ViralTreeSwatches
* Sketch should open up and the plugin should be installed now.