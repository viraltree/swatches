var isValidHexRegex = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;
var inputTitle = 'Please enter a sequence of hexadecimal values';
var exampleMessage = '#91B6AF, #7D6962';

function onRun(ctx) {
  var doc = ctx.document;
  var page = [doc currentPage];
  var currentArtboard = [page currentArtboard];
  var canvas = currentArtboard ? currentArtboard : page;

  var hexInput = doc.askForUserInput_initialValue(inputTitle, exampleMessage);

  var validInput = validateInput(hexInput);
  
  if(validInput.length < 1) {
    return;
  }

  var splitHexValues = hexInput.split(',');
  var swatchesGroup = createGroup(canvas, splitHexValues);
  addSwatches(swatchesGroup, splitHexValues);
}

function validateInput(hexInput) {
  return hexInput
    .split(',')
    .map(trim)
    .filter(isValidHex);
}

function trim(hex) {
  return hex.trim();
}

function isValidHex(hex) {
  return isValidHexRegex.test(hex);
}

function addSwatches(swatchesGroup, hexInput) {
  for(var i = 0; i < hexInput.length; i += 1) {
    addSwatch(swatchesGroup, hexInput[i].trim(), i);
  }
}

function addSwatch(swatchesGroup, hexadecimal, count) {

  var swatchGroup = swatchesGroup.addLayerOfType("group");
      swatchGroup.setName("Swatch: " + hexadecimal);
      swatchGroup.frame().x = count * 150;
      swatchGroup.frame().y = 0;
      swatchGroup.frame().width = 150;
      swatchGroup.frame().height = 180;

  var colorBlock = swatchGroup.addLayerOfType("rectangle");
  if (colorBlock.embedInShapeGroup != undefined) {
    colorBlock = colorBlock.embedInShapeGroup()
  }

  colorBlock.frame().x = 0;
  colorBlock.frame().y = 0;
  colorBlock.frame().width = 130;
  colorBlock.frame().height = 130;
  colorBlock.setName(hexadecimal);

  var colorBlockFill = colorBlock.style().addStylePartOfType(0);//0 == fill
  colorBlockFill.color = MSColor.colorWithSVGString(hexadecimal);

  var colorName = swatchGroup.addLayerOfType("text");
  colorName.stringValue = "Color name";
  colorName.name = "Color name";
  colorName.fontPostscriptName = "ProximaNova-Bold";
  colorName.fontSize = 15;
  colorName.textColor = MSColor.colorWithSVGString("#444");
  colorName.frame().x = 0;
  colorName.frame().y = 137;

  var colorText = swatchGroup.addLayerOfType("text");
  colorText.stringValue = hexadecimal;
  colorText.name = hexadecimal;
  colorName.fontPostscriptName = "ProximaNova-Regular";
  colorText.fontSize = 13;
  colorText.textColor = MSColor.colorWithSVGString("#777");
  colorText.frame().x = 0;
  colorText.frame().y = 159;

  var rgb = hexToRgb(hexadecimal);
  var colorTextRGB = swatchGroup.addLayerOfType("text");
  colorTextRGB.stringValue = "rgb(" + rgb.r +", "+ rgb.g + ", " + rgb.b + ")";
  colorTextRGB.name = "RGB Value";
  colorTextRGB.fontPostscriptName = "ProximaNova-Regular";
  colorTextRGB.fontSize = 14;
  colorTextRGB.textColor = MSColor.colorWithSVGString("#777");
  colorTextRGB.frame().x = 0;
  colorTextRGB.frame().y = 179;
}

function hexToRgb(hex) {
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function(m, r, g, b) {
      return r + r + g + g + b + b
  });
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  
  return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
  } : null;
}

function createGroup(canvas, hexInput) {
  var swatchesLen = hexInput.length;
  var swatchesGroup = canvas.addLayerOfType("group");
  swatchesGroup.setName('Swatches - ' + swatchesLen + ' colors');
  swatchesGroup.frame().x = 0;
  swatchesGroup.frame().y = 0;
  swatchesGroup.frame().width = swatchesLen * 150;
  swatchesGroup.frame().height = 180;
  return swatchesGroup;
}